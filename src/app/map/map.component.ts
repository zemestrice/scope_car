import { Component, AfterViewInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import * as L from "leaflet";
import { EMarker } from '../models/extended-marker';
import { VehicleInfo, VehicleUser } from '../models/user';
import { Vehicle } from '../models/vehicle';
import { TrackingService } from '../tracking.service';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit, OnChanges {
  @Input() vehicles: Vehicle[];
  @Input() data: VehicleUser;
  @Input() selectedVehicle: Vehicle;
  @Output() selectedVehicleEvent = new EventEmitter<Vehicle>();
  @Output() mapLoadingDoneEvent = new EventEmitter();
  @Output() userNotLoadedEvent = new EventEmitter();

  private map: L.Map;
  private mapMarkers: EMarker[] = [];
  private vehicleDataParsed: number;

  constructor(private _trackingService: TrackingService) {

  }

  private initMap(): void {
    this.map = L.map('map', {
      zoom: 11
    });

    this.vehicleDataParsed = 0;

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
    let vList = this.vehicles.filter(v => v.lat && v.lon);
    vList.forEach(v => {
      let d = this.data.vehicles.find(ve => ve.vehicleid == v.vehicleid);
      this._trackingService.getLatLongAddress(v.lat, v.lon)
        .subscribe((address: any) => {
          this.createMarker(v, d, address);
          this.vehicleDataParsed++;
          if (this.vehicleDataParsed >= vList.length) {
            this.panAllMarkersIntoView();
            this.mapLoadingDoneEvent.emit();
          }
        });
    });
    if (vList.length == 0) {
      setTimeout(() =>
        this.mapLoadingDoneEvent.emit(), 100);
    }
    this.map.panTo(new L.LatLng(56.946285, 24.105078));

  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.map && this.updateMarkers()
  }


  panAllMarkersIntoView() {
    var bounds = L.latLngBounds(this.mapMarkers
      .map(d => L.latLng(d.vehicle.lat, d.vehicle.lon))
    );
    this.map.flyToBounds(bounds);
  }

  createMarker(vCoords: Vehicle, vInfo: VehicleInfo, address: string): void {
    let marker = L.marker([vCoords.lat, vCoords.lon]) as EMarker;
    marker.vehicle = vCoords;
    marker.bindPopup(`
          <div><img width="150px" height="150px" src="${vInfo.foto}"</div>
          <div>Name: ${vInfo.make} ${vInfo.model}</div>
          <div>Address: ${address} </div>
          `);
    marker.setIcon(L.divIcon({
      className: "marker-container",
      iconAnchor: [0, 24],
      popupAnchor: [0, -36],
      html: `<span class="marker" style="background-color: ${vInfo.color}" />`
    }))
    marker.addTo(this.map).on('click', () => {
      this.selectedVehicleEvent.emit(vCoords);
    });
    this.addMarker(marker, vCoords);
  }

  addMarker(marker: EMarker, v: Vehicle) {
    let mi = this.mapMarkers.findIndex(d => d.vehicle.vehicleid == v.vehicleid);
    if (mi > -1) {
      this.mapMarkers[mi].vehicle = v;
      this.mapMarkers[mi].setLatLng(L.latLng(v.lat, v.lon))
      return;
    }
    this.mapMarkers.push(marker);
  }

  updateMarkers() {
    if (!this.data) {
      this.userNotLoadedEvent.emit();
    }

    for (let index = 0; index < this.mapMarkers.length; index++) {
      const mv = this.mapMarkers[index];
      let mi = this.vehicles.find(d => d.vehicleid == mv.vehicle.vehicleid);
      if (mi && mi.isValid()) {
        mv.vehicle = mi;
        mv.setLatLng(L.latLng(mv.vehicle.lat, mv.vehicle.lon));
        return;
      }
      this.map.removeLayer(mv);
      mv.remove();
      this.mapMarkers.splice(index, 1);
    }

    this.vehicles.filter(f => f.isValid()).forEach(v => {
      let mi = this.mapMarkers.findIndex(d => d.vehicle.vehicleid == v.vehicleid);
      if (mi == -1) {
        this._trackingService.getLatLongAddress(v.lat, v.lon)
          .subscribe((address: string) => {
            let d = this.data.vehicles.find(ve => ve.vehicleid == v.vehicleid);
            this.createMarker(v, d, address);
            this.vehicleDataParsed++;
            if (this.vehicleDataParsed >= this.vehicles.filter(v => v.isValid()).length) {
              this.panAllMarkersIntoView();
              this.mapLoadingDoneEvent.emit();
            }
          });
      }
      if (this.selectedVehicle && this.selectedVehicle.vehicleid == v.vehicleid) {
        this.map.panTo(new L.LatLng(
          this.selectedVehicle.lat,
          this.selectedVehicle.lon)
        );
      }
    });
  }

  highlight(v: Vehicle) {
    this.selectedVehicle = v;
    this.updateMarkers();
    if (!v.isValid()) {
      return;
    }
    this.map.flyTo([v.lat, v.lon]);
    let m = this.mapMarkers.findIndex(d => d.vehicle.vehicleid == v.vehicleid);
    if (m > -1) {
      let popup = this.mapMarkers[m];
      popup.isPopupOpen() ? popup.closePopup() : popup.openPopup();
      return;
    }
  }
}
