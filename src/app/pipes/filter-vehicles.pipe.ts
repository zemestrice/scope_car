import { Pipe, PipeTransform } from '@angular/core';
import { Vehicle } from '../models/vehicle';

@Pipe({
  name: 'filterValidVehicles'
})
export class FilterVehiclesPipe implements PipeTransform {

  transform(value: Vehicle[]): Vehicle[] {
    return value?.filter(d => d && d.isValid());
  }

}
