import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpResponse,
  HttpEventType
} from '@angular/common/http';
import { of, tap } from 'rxjs';
import { CacheMapService } from '../../services/cache-map-service.service'
import { TrackingService } from 'src/app/tracking.service';



@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  private _cacheUrlList: Map<string, number> = new Map<string, number>();

  constructor(private cache: CacheMapService) {
      this._cacheUrlList.set(TrackingService.userListUrl, 5 * 60000);
      this._cacheUrlList.set(TrackingService.vehicleListUrl, 30 * 1000);
   }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let maxAge = this.canBeCached(req);
    if (maxAge < 1) {
      return next.handle(req);
    }
    const cachedResponse = this.cache.get(req);
    if (cachedResponse !== null) {
      return of(cachedResponse.clone());
    }
    return next.handle(req).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          if (event.headers.get("Content-Type") != 'application/json') {
            try {
              JSON.parse(event.body as string);
            }
            catch {
              return;
            }
          }
          if (event.status == 200 && event.type == HttpEventType.Response) {
            this.cache.put(req, event, maxAge);
          }
        }
      })
    );
  }

  private findCacheKey(url: string) {
    return [...this._cacheUrlList.keys()].find(d => url.indexOf(d) > -1)
  }

  private canBeCached(req: HttpRequest<any>): number {
    if (req.method !== 'GET') return -1;
    
    var key = this.findCacheKey(req.urlWithParams);
    if (key) {
      return this._cacheUrlList.get(key);
    }
    return -1;
  }
}
