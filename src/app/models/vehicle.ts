export class Vehicle {
  vehicleid: number;
  lat: number;
  lon: number;

  constructor(v: Vehicle) {
    this.vehicleid = v.vehicleid;
    this.lat = v.lat;
    this.lon = v.lon;
  }

  isValid(): boolean {
    return this.lat != null && this.lon != null;
  };
}

export interface VehicleListResponse {
  data: Vehicle[];
  error: string;
}
