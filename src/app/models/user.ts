export interface VehicleUser {
  userid: string;
  owner: Owner;
  vehicles: VehicleInfo[];
}
export interface Owner {
  name: string;
  surname: string;
  foto: string;
}

export interface VehicleInfo {
  vehicleid: number;
  make: string;
  model: string;
  year: string;
  color: string;
  vin: string;
  foto: string;
}
export interface UserListResponse {
  data: VehicleUser[];
}
