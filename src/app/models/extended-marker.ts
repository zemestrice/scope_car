import { Vehicle } from "./vehicle";

export interface EMarker extends L.Marker {
	vehicle: Vehicle;
}