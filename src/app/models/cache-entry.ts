import { HttpResponse } from '@angular/common/http';

export interface CacheEntry {
    urlWithParams: string;
    response: HttpResponse<any>
    entryTime: number;
	maxAge: number; 
}
