import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, shareReplay, switchMap } from 'rxjs';
import { UserListResponse } from './models/user';
import { Vehicle, VehicleListResponse } from './models/vehicle';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
@Injectable()
export class TrackingService {
  static userListUrl = 'https://mobi.connectedcar360.net/api/?op=list';
  static vehicleListUrl = 'https://mobi.connectedcar360.net/api/?op=getlocations&userid=';
  static reverseGeoLocUrl = 'https://nominatim.openstreetmap.org/reverse?format=jsonv2';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  rand: number;
  constructor(private _http: HttpClient, private _snackBar: MatSnackBar) { }

  showError(errMsg: string): void {
    this._snackBar.open(errMsg, 'Error', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  getUserList(): Observable<UserListResponse> {
    return this._http.get<UserListResponse>(TrackingService.userListUrl).pipe(
      catchError((err: Error) => {
        console.log(err);
        this.showError(err.message);
        return of({ data: [] } as UserListResponse)
      })
    );
  }
  getVehicleList(userId: string): Observable<VehicleListResponse> {
    const requestOptions: Object = {
      //observe: 'response',
      responseType: 'text'
    }
    return this._http.get<string>(TrackingService.vehicleListUrl + userId, requestOptions)
    .pipe(
      switchMap(resp => {
        if (resp.indexOf("User doesn't exist") > -1 ) {
          throw  "User doesn't exist";
        }
        return of(JSON.parse(resp) as VehicleListResponse)
      }),
      map(res => {
        return {
          data: res.data.map(v => {
            return new Vehicle(v)
          })
        } as VehicleListResponse
      }),
      catchError((err) => {
        console.log(err);
        this.showError(err && err.message ? err.message : err);
        if (err instanceof SyntaxError) {
          throw err;
        }
        return of({ data: null, error: err } as VehicleListResponse);
      })
    );
  }

  getLatLongAddress(lat: number, lon: number): Observable<string> {
    return this._http.get(`${TrackingService.reverseGeoLocUrl}&lat=${lat}&lon=${lon}`)
      .pipe(
        map((resp: any) => resp && resp.name),
        catchError(err => {
          console.error(err);
          return of('');
        }))
  }
}
