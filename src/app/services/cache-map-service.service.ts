import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CacheEntry } from '../models/cache-entry';

@Injectable({
  providedIn: 'root'
})
export class CacheMapService {

  cacheMap = new Map<string, CacheEntry>();
    get(req: HttpRequest<any>): HttpResponse<any> | null {
        const entry = this.cacheMap.get(req.urlWithParams);
        if (!entry) {
            return null;
        }
        const isExpired = (Date.now() - entry.entryTime) > entry.maxAge;
        if (isExpired) {
            this.cacheMap.delete(req.urlWithParams);
        }
        return isExpired ? null : entry.response;
    }
    put(req: HttpRequest<any>, res: HttpResponse<any>, maxAgeMilliseconds: number): void {
        const entry: CacheEntry = { urlWithParams: req.urlWithParams, response: res, entryTime: Date.now(), maxAge: maxAgeMilliseconds };
        this.cacheMap.set(req.urlWithParams, entry);
        this.deleteExpiredCache();
    }
    private deleteExpiredCache() {
        this.cacheMap.forEach(entry => {
            if ((Date.now() - entry.entryTime) > entry.maxAge) {
                this.cacheMap.delete(entry.urlWithParams);
            }
        });
    }
}
