import { TestBed } from '@angular/core/testing';
import { CacheMapService } from './cache-map-service.service';

describe('CacheMapServiceService', () => {
  let service: CacheMapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CacheMapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
