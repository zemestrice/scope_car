import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, combineLatest, delayWhen, finalize, forkJoin, interval, last, map, Observable, of, retry, RetryConfig, retryWhen, switchMap, tap, timer, withLatestFrom } from 'rxjs';
import { MapComponent } from '../map/map.component';
import { VehicleInfo, VehicleUser } from '../models/user';
import { Vehicle, VehicleListResponse } from '../models/vehicle';
import { TrackingService } from '../tracking.service';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {
  @ViewChild(MapComponent) map: MapComponent;

  userId: string = '';
  vehicles$: Observable<VehicleListResponse>;
  displayedColumns = ['vehicleid', 'lat', 'lon'];
  user$: Observable<VehicleUser>;
  selectedVehicle: Vehicle;
  isLoading: boolean = true;
  comb$: Observable<VehicleListResponse>;

  constructor(
    private _userService: TrackingService,
    private _activeRoute: ActivatedRoute,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.userId = this._activeRoute.snapshot.paramMap.get('userId') || '';
    this.user$ = this._userService.getUserList().pipe(
      map(d => {
        let user = d.data?.find(u => u.userid == this.userId);
        if (user) {
          return user;
        }
        throw 'No user data found'
      }),
      retry({
        count: 2,
        delay: 1000
      } as RetryConfig
      )
    );

    this.vehicles$ = timer(0, 60000).pipe(
      switchMap(_ =>
        this._userService.getVehicleList(this.userId).pipe(
          map((vListResponse: VehicleListResponse) => {
            if (vListResponse && vListResponse.data != null) {
              return vListResponse;
            }
            throw vListResponse && vListResponse.error ? vListResponse.error : 'No Vehicle data found'
          }),
          retry({
            count: 2,
            delay: 1000
          } as RetryConfig
          ),
          catchError((err) => { 
            this._router.navigate(['/']);
            return of({} as VehicleListResponse);
          })
        )
      ));
  }

  pageLoaded() {
    this.isLoading = false;
  }

  getCarInfo(vId: number, vehicles: VehicleInfo[]):VehicleInfo {
    return vehicles.find(v => v.vehicleid == vId);
  }

  highlightOnMap(vehicle: Vehicle) {
    this.highlightVehicle(vehicle);
    this.map.highlight(vehicle);
  }

  highlightVehicle(vehicle: Vehicle): void {
    if (this.selectedVehicle && this.selectedVehicle.vehicleid == vehicle.vehicleid) {
      this.selectedVehicle = null;
      return;
    }

    this.selectedVehicle = vehicle;
  }
}