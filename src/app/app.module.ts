import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TrackingService } from './tracking.service';
import { MapViewComponent } from './map-view/map-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { MatTableModule } from '@angular/material/table';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MapComponent } from './map/map.component';
import { CacheInterceptor } from './interceptors/cache/cache.interceptor';
import { NavigationComponent } from './navigation/navigation.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { MatExpansionModule } from '@angular/material/expansion';
import { LoadingComponent } from './loading/loading.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { FilterVehiclesPipe } from './pipes/filter-vehicles.pipe';
@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    MapViewComponent,
    MapComponent,
    NavigationComponent,
    LoadingComponent,
    FilterVehiclesPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatButtonModule,
    DragDropModule,
    BrowserAnimationsModule,
    NgxJsonViewerModule
  ],
  providers: [TrackingService, { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
