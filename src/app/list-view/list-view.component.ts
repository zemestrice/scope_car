import { Component, OnInit } from '@angular/core';
import { map, Observable, retry, RetryConfig } from 'rxjs';
import { VehicleUser, UserListResponse } from '../models/user';
import { TrackingService } from '../tracking.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  userList$: Observable<UserListResponse>;
  displayedColumns = ['userId', 'name', 'photo', 'vehicleCount'];


  constructor(private userService: TrackingService) {
    this.userList$ = this.userService.getUserList()
      .pipe(
        map((resp: UserListResponse) => {
          if (resp && resp.data.length > 0) {
            return resp;
          }
          throw 'User list empty!';
        }),
        retry({
          count: 2,
          delay: 1000
        } as RetryConfig)
      )
  }

  ngOnInit() {

  }

  hasData(index, item: VehicleUser): boolean {
    return item.owner && item.userid && item.vehicles && item.vehicles.length > 0;
  }

  noData(index, item: VehicleUser): boolean {
    return !item.owner || !item.userid || !item.vehicles || item.vehicles.length == 0;
  }
}